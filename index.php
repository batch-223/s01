<!-- <?php require_once './code.php'; ?> -->
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity S01</title>
</head>

<body>
	<h1>Full Address</h1>
	<p><?php echo getFullAddress('Philippines', 'Iloilo City', 'Leon', 'Brgy. Talacuan') ?></p>
	<p><?php echo getFullAddress('Philippines', 'Cebu City', 'Basak', 'East Ridge') ?></p>

	<h1>Letter-Based Grading</h1>
	<p><?php echo getLetterGrade(98) ?></p>
	<p><?php echo getLetterGrade(87) ?></p>
	<p><?php echo getLetterGrade(71) ?></p>
	<p><?php echo getLetterGrade(83) ?></p>
	<p><?php echo getLetterGrade(85) ?></p>
	<p><?php echo getLetterGrade(95) ?></p>
</body>

</html>